clear all;
plot_type = 1; % 0 - plot TX Power, 1 - plot RX Power
data_range = 10000;
s = serialport("COM5", 115200);
data = [];

Animated_Plot_rx = animatedline;
Sample_Index = 1;
rp0 = 0;
rp1 = 0;
rp2 = 0;
rp3 = 0;
rp4 = 0;
tp_slope = 0;
tp_offset = 0;
data_point = 0;

while true
    
    data = read_data(s);
    [data_type, value] = get_data_from_frame(data);
    
    if data_type == 3
        tp_slope = value;
    elseif data_type == 4
        tp_offset = value;
    elseif data_type == 5
        rp0 = value;
    elseif data_type == 6
        rp1 = value;
    elseif data_type == 7
        rp2 = value;
    elseif data_type == 8
        rp3 = value;
    elseif data_type == 9
        rp4 = value;      
    elseif data_type == plot_type
        
        if plot_type == 0
            data_point = (value*tp_slope + tp_offset)/10;
        elseif plot_type == 1
            data_point = ((value^4)*rp4 + (value^3)*rp3 + (value^2)*rp2 + value*rp1 + rp0)/10;
        end
        
        hold on           
        addpoints(Animated_Plot_rx, Sample_Index, data_point);
        drawnow
        Window = 500;
        Sample_Chunk = floor(Sample_Index/Window);
        Maximum_Amplitude = data_range;
        Minimum_Amplitude = 0;
        axis([Sample_Chunk*Window Sample_Chunk*Window+Window Minimum_Amplitude Maximum_Amplitude]);
        
        if plot_type == 0
            title("TX Power");
        elseif plot_type == 1
            title("RX Power");
        end 
        
        xlabel("Sample Index"); ylabel("Amplitude[uW]");
        Sample_Index = Sample_Index + 1;
        grid on
    end
end

function [A] = read_data(s)

    sync_byte = 0;
    
    while sync_byte ~= 0xAA
        sync_byte = read(s, 1, "uint8");
    end
        
    A = read(s, 3, "uint8");
end

function [data_type, data] = get_data_from_frame(A)
    data_type = A(1);
    data = bitor(bitshift(A(2), 8), A(3)); 
end